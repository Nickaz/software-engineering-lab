#include "lnx.h"
/*!
 * \file
 * \brief Описание функции, вычисляющей значение натурального логарифма от 1 + икс
 */

/*!
 * \param[in] x значение икс
 * \param[in] e точность (чем меньше, тем точнее)
 * \return res результат вычисления
*/
double lnX(double x, double e)
{
    long double curNumerator = x;
    long long int curDenominator = 1;
    int sign = -1;
    double add = 0;
    double res = x;
    do
    {
        curNumerator *= x;
        curDenominator++;
        add = curNumerator / curDenominator * sign;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
        sign = -sign;
    } while ((add>=e) || (add<=-e));
    return res;
}
