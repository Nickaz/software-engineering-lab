#ifndef ARCTGX_H
#define ARCTGX_H
//! \file
//! \brief Функция для вычисления арктангенса
double arctgX(double x, double e);

#endif // ARCTGX_H
