#include "sinhx.h"
/*!
 * \file
 * \brief Описание функции, вычисляющей значение гиперболического синуса
 */

/*!
 * \param[in] x значение икс
 * \param[in] e точность (чем меньше, тем точнее)
 * \return res результат вычисления
*/
double sinhX(double x,  double e)
{
    long double curNumerator = x;
    long long int curDenominator = 1;
    int n = 1;
    double add = 0;
    double res = x;
    do
    {
        curNumerator *= x * x;
        curDenominator *= (n+1) * (n+2);
        add = curNumerator / curDenominator;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
        n = n+2;
    } while ((add>=e) || (add<=-e));
    return res;
}
