#include "xina.h"
/*!
 * \file
 * \brief Описание функции, вычисляющей значение показательной функции
 */

/*!
 * \param[in] x значение икс
 * \param[in] e точность (чем меньше, тем точнее)
 * \param[in] a число альфа - степень
 * \return res результат вычисления
*/
double xInA(double x, double e, double a)
{
    double curNumerator = 1;
    int curDenominator = 1;
    double curA = a;
    double add = 0;
    int n = 1;
    double res = 1;
    do
    {
        curNumerator *= x;
        curDenominator *= n;
        add = curA * curNumerator / curDenominator ;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
        curA = curA * (a - n);
        n++;
    } while ((add>=e) || (add<=-e));
    return res;
}
