#include "einx.h"
/*!
 * \file
 * \brief Описание функции, вычисляющей значение экспоненты в степени икс
 */

/*!
 * \param[in] x значение икс
 * \param[in] e точность (чем меньше, тем точнее)
 * \return res результат вычисления
*/
double eInX(double x, double e)
{
    long double curNumerator = 1;
    long long int curDenominator = 1;
    int n = 1;
    double add = 0;
    double result = 1;
    do
    {
        curNumerator *= x;
        curDenominator *= n;
        add = curNumerator / curDenominator;
        if (add >= e)
        {
            result += add;
        }
        n++;
    } while (add >= e);
    return result;
}
