#ifndef LNX_H
#define LNX_H
//! \file
//! \brief Функция для вычисления натурального логарифма от 1 + икс
double lnX(double x, double e);

#endif // LNX_H
