#include "sinx.h"
/*!
 * \file
 * \brief Описание функции, вычисляющей значение косинуса
 */

/*!
 * \param[in] x значение икс
 * \param[in] e точность (чем меньше, тем точнее)
 * \return res результат вычисления
*/
double sinX(double x, double e)
{
    long double curNumerator = x;
    long long int curDenominator = 1;
    int n = 1;
    int sign = -1;
    double add = 0;
    double res = x;
    do
    {
        curNumerator *= x * x;
        curDenominator *= (n+1) * (n+2);
        add = curNumerator / curDenominator * sign;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
        n = n+2;
        sign = -sign;
    } while ((add>=e) || (add<=-e));
    return res;
}
