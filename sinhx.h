#ifndef SINHX_H
#define SINHX_H
//! \file
//! \brief Функция для вычисления гиперболического синуса
double sinhX(double x,  double e);

#endif // SINHX_H
