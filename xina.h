#ifndef XINA_H
#define XINA_H
//! \file
//! \brief Функция для вычисления икс в степени альфа
double xInA(double x, double e, double a);

#endif // XINA_H
