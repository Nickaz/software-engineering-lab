#ifndef COSX_H
#define COSX_H
//! \file
//! \brief Функция для вычисления косинуса
double cosX(double x, double e);

#endif // COSX_H
