#include <QtTest>
#include "tst_function_test.h"
// add necessary includes here

function_test::function_test()
{

}


void function_test::test_eInX()
{
    QCOMPARE(eInX(1, 1), 2);
    QVERIFY(qFuzzyCompare(2.5+1.0/6, eInX(1, 0.05)));
    QVERIFY(qFuzzyCompare(12.181743191243, eInX(2.5, 0.002)));
}

void function_test::test_sinX()
{
    QCOMPARE(sinX(0,0.1), 0);
    QVERIFY(qFuzzyCompare(1-1.0/6+1.0/120, sinX(1, 0.008)));
    QVERIFY(qFuzzyCompare(0.5990461991998, sinX(2.5, 0.002)));
}

void function_test::test_cosX()
{
    QCOMPARE(cosX(0, 0.1), 1);
    QVERIFY(qFuzzyCompare(0.5+1.0/24, cosX(1, 0.008)));
    QVERIFY(qFuzzyCompare(-0.8012638865745, cosX(2.5, 0.002)));
}

void function_test::test_lnX()
{
    QCOMPARE(lnX(0, 0.1), 0);
    QVERIFY(qFuzzyCompare(0.359375+0.125/3, lnX(0.5, 0.008)));
    QVERIFY(qFuzzyCompare(-0.6911458333333, lnX(-0.5, 0.002)));
}

void function_test::test_xInA()
{
    QCOMPARE(xInA(0, 0.1, 1), 1);
    QVERIFY(qFuzzyCompare(3.656, xInA(0.5, 0.008, 3.2)));
    QVERIFY(qFuzzyCompare(0.25, xInA(-0.5, 0.002, 2)));
}

void function_test::test_arctgX()
{
    QCOMPARE(arctgX(0, 0.1), 0);
    QVERIFY(qFuzzyCompare(0.5-0.125/3, arctgX(0.5, 0.008)));
    QVERIFY(qFuzzyCompare(-0.32+0.32*0.32*0.32/3, arctgX(-0.32, 0.002)));
}
void function_test::test_oneDivX()
{
    QCOMPARE(oneDivX(0, 0.1), 1);
    QVERIFY(qFuzzyCompare(1.984375 ,oneDivX(0.5, 0.008)));
    QVERIFY(qFuzzyCompare(0.7567623168, oneDivX(-0.32, 0.002)));
}
void function_test::test_sinhX()
{
    QCOMPARE(sinhX(0, 0.1), 0);
    QVERIFY(qFuzzyCompare(0.5+0.125/6, sinhX(0.5, 0.008)));
    QVERIFY(qFuzzyCompare(-0.32+(-0.032768/6), sinhX(-0.32, 0.002)));
}

QTEST_APPLESS_MAIN(function_test)

