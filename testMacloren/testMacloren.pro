QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_function_test.cpp \
    ../arctgx.cpp \
    ../cosx.cpp \
    ../einx.cpp \
    ../lnx.cpp \
    ../sinx.cpp \
    ../xina.cpp \
    ../onedivx.cpp \
    ../sinhx.cpp


HEADERS += \
    tst_function_test.h \
    ../einx.h \
    ../sinx.h \
    ../cosx.h \
    ../lnx.h \
    ../xina.h \
    ../arctgx.h \
    ../onedivx.h \
    ../sinhx.h


