#ifndef TST_FUNCTION_TEST_H
#define TST_FUNCTION_TEST_H

#include <QtTest>
#include "../xina.h"
#include "../sinx.h"
#include "../lnx.h"
#include "../einx.h"
#include "../cosx.h"
#include "../arctgx.h"
#include "../onedivx.h"
#include "../sinhx.h"

class function_test : public QObject
{
    Q_OBJECT

public:
    function_test();

private slots:
    void test_eInX();
    void test_sinX();
    void test_cosX();
    void test_lnX();
    void test_xInA();
    void test_arctgX();
    void test_oneDivX();
    void test_sinhX();
};

#endif // TST_FUNCTION_TEST_H
