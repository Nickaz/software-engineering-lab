#ifndef EINX_H
#define EINX_H
//! \file
//! \brief Функция для вычисления экспоненты в степени икс
double eInX(double x, double e);

#endif // EINX_H
