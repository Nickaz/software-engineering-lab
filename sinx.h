#ifndef SINX_H
#define SINX_H
//! \file
//! \brief Функция для вычисления синуса
double sinX(double x, double e);

#endif // SINX_H
