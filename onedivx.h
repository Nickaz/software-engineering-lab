#ifndef ONEDIVX_H
#define ONEDIVX_H
//! \file
//! \brief Функция для вычисления отношения 1 к 1 - Х
double oneDivX(double x, double e);

#endif // ONEDIVX_H
