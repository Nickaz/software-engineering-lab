#include "onedivx.h"
/*!
 * \file
 * \brief Описание функции, вычисляющей значение функции 1 делить на 1 - икс
 */

/*!
 * \param[in] x значение икс
 * \param[in] e точность (чем меньше, тем точнее)
 * \return res результат вычисления
*/
double oneDivX(double x, double e)
{
    long double cur = 1;
    double add = 0;
    double result = 1;
    do
    {
        cur *= x;
        add = cur;
        if ((add >= e) || (add <= -e))
        {
            result += add;
        }
    } while ((add >= e) || (add <= -e));
    return result;
}
