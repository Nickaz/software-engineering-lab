#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSize>
#include <QDoubleSpinBox>

#include "xina.h"
#include "sinx.h"
#include "lnx.h"
#include "einx.h"
#include "cosx.h"
#include "arctgx.h"
#include "onedivx.h"
#include "sinhx.h"
//! \file
//! \brief Заголовочный файл для кода графического интерфейса
//!
class Widget : public QWidget
{
    Q_OBJECT
    QPushButton *pb_eInX;
    QPushButton *pb_sinX;
    QPushButton *pb_cosX;
    QPushButton *pb_lnOnePlusX;
    QPushButton *pb_onePlusXinA;
    QPushButton *pb_arctgX;
    QPushButton *pb_calculate;
    QPushButton *pb_onedivx;
    QPushButton *pb_sinhx;

    QLabel *lbl_buttonsTitle;
    QLabel *lbl_resultTitle;
    QLabel *lbl_selectedFunction;

    QLabel *lbl_xValue;
    QLabel *lbl_eValue;
    QLabel *lbl_aValue;

    QDoubleSpinBox *sb_xValue;
    QDoubleSpinBox *sb_eValue;
    QDoubleSpinBox *sb_aValue;
    QLineEdit *le_result;

    QVBoxLayout *bl_buttons;
    QVBoxLayout *bl_result;
    QHBoxLayout *bl_lblValues;
    QHBoxLayout *bl_values;

    QHBoxLayout *bl_mainLayout;

    qint16 selectedFunction = 0; //!<Содержит номер выбранной функции из enum func

    enum func
    {
        nothing,
        einx,
        sinx,
        cosx,
        lnx,
        xina,
        arctgx,
        onedivx,
        sinhx
    };

public:
    Widget(QWidget *parent = nullptr);
    void showWidgets(int func);


    ~Widget();
public slots:
    void exFuncClicked();
    void sinFuncClicked();
    void cosFuncClicked();
    void lnFuncClicked();
    void degFuncClicked();
    void arctgFuncClicked();
    void calculateClicked();
    void oneDivXFuncClicked();
    void sinhFuncClicked();

};

#endif // WIDGET_H
