#include "arctgx.h"
/*!
 * \file
 * \brief Описание функции, вычисляющей значение арктангенса
 */

/*!
 * \param[in] x значение икс
 * \param[in] e точность (чем меньше, тем точнее)
 * \return res результат вычисления
*/
double arctgX(double x, double e)
{
    double curNumerator = x;
    int curDenominator = 1;
    int sign = -1;
    double add = 0;
    double res = x;
    do
    {
        curNumerator *= x * x;
        curDenominator += 2;
        add = curNumerator / curDenominator * sign;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
        sign = -sign;
    } while ((add>=e) || (add<=-e));
    return res;
}
