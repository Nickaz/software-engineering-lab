#include "widget.h"
/*!
 * \file
 * \brief Файл с кодом окна - графическая составляющая приложения
 */
Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    pb_eInX = new QPushButton();
    pb_sinX = new QPushButton();
    pb_cosX = new QPushButton();
    pb_lnOnePlusX = new QPushButton();
    pb_onePlusXinA = new QPushButton();
    pb_arctgX = new QPushButton();
    pb_calculate = new QPushButton("Рассчитать");
    pb_calculate->setDisabled(true);
    pb_onedivx = new QPushButton("1/(1-x)");
    pb_sinhx = new QPushButton("sinhx");

    lbl_buttonsTitle = new QLabel("Выберите фунцкцию:");
    lbl_resultTitle = new QLabel("Значение функции:");
    lbl_resultTitle->setAlignment(Qt::AlignHCenter);
    lbl_selectedFunction = new QLabel("**Не выбрано**");
    lbl_selectedFunction->setAlignment(Qt::AlignHCenter);

    lbl_xValue = new QLabel("Значене Х:");
    lbl_eValue = new QLabel("Точность:");
    lbl_aValue = new QLabel("Значене А:");
    lbl_xValue->setAlignment(Qt::AlignBottom);
    lbl_eValue->setAlignment(Qt::AlignBottom);
    lbl_aValue->setAlignment(Qt::AlignBottom);
    lbl_xValue->hide();
    lbl_aValue->hide();
    lbl_eValue->hide();

    sb_xValue = new QDoubleSpinBox();
    sb_eValue = new QDoubleSpinBox();
    sb_aValue = new QDoubleSpinBox();

    //! Прячем спинбоксы до того, как будет выбрана функция
    sb_xValue->hide();
    sb_eValue->hide();
    sb_aValue->hide();
    sb_eValue->setDecimals(4);
    sb_eValue->setRange(0.0001, 100);
    sb_aValue->setDecimals(3);
    sb_xValue->setDecimals(3);

    le_result = new QLineEdit();
    le_result->setReadOnly(true);

    bl_buttons = new QVBoxLayout();
    bl_result = new QVBoxLayout();
    bl_lblValues = new QHBoxLayout();
    bl_values = new QHBoxLayout();
    bl_mainLayout = new QHBoxLayout();

    /*!
     * Для каждой кнопки устанавливается изображение
     */

    QPixmap pix("X:\\QtProjects\\Macloren\\ex.png");

    pb_eInX->setIcon(QIcon(pix));
    pb_eInX->setIconSize(QSize(50, 40));
    pb_eInX->setFixedSize(QSize(140, 40));

    pix.load("X:\\QtProjects\\Macloren\\sin.png");
    pb_sinX->setIcon(QIcon(pix));
    pb_sinX->setIconSize(QSize(45,35));
    pb_sinX->setFixedSize(QSize(140, 40));

    pix.load("X:\\QtProjects\\Macloren\\cos.png");
    pb_cosX->setIcon(QIcon(pix));
    pb_cosX->setIconSize(QSize(50,40));
    pb_cosX->setFixedSize(QSize(140, 40));

    pix.load("X:\\QtProjects\\Macloren\\ln.png");
    pb_lnOnePlusX->setIcon(QIcon(pix));
    pb_lnOnePlusX->setIconSize(QSize(60,50));
    pb_lnOnePlusX->setFixedSize(QSize(140, 40));

    pix.load("X:\\QtProjects\\Macloren\\degA.png");
    pb_onePlusXinA->setIcon(QIcon(pix));
    pb_onePlusXinA->setIconSize(QSize(60,50));
    pb_onePlusXinA->setFixedSize(QSize(140, 40));

    pix.load("X:\\QtProjects\\Macloren\\arctg.png");
    pb_arctgX->setIcon(QIcon(pix));
    pb_arctgX->setIconSize(QSize(50,40));
    pb_arctgX->setFixedSize(QSize(140, 40));

    bl_buttons->addWidget(lbl_buttonsTitle);
    bl_buttons->addWidget(pb_eInX);
    bl_buttons->addWidget(pb_sinX);
    bl_buttons->addWidget(pb_cosX);
    bl_buttons->addWidget(pb_lnOnePlusX);
    bl_buttons->addWidget(pb_onePlusXinA);
    bl_buttons->addWidget(pb_arctgX);
    bl_buttons->addWidget(pb_onedivx);
    bl_buttons->addWidget(pb_sinhx);

    bl_lblValues->addWidget(lbl_xValue);
    bl_lblValues->addWidget(lbl_eValue);
    bl_lblValues->addWidget(lbl_aValue);

    bl_values->addWidget(sb_xValue);
    bl_values->addWidget(sb_eValue);
    bl_values->addWidget(sb_aValue);

    bl_result->addWidget(lbl_resultTitle);
    bl_result->addWidget(lbl_selectedFunction);
    bl_result->addLayout(bl_lblValues);
    bl_result->addLayout(bl_values);
    bl_result->addWidget(pb_calculate);
    bl_result->addWidget(le_result);

    bl_mainLayout->addLayout(bl_buttons);
    bl_mainLayout->addLayout(bl_result);

    setLayout(bl_mainLayout);

    connect(pb_eInX, SIGNAL(clicked()), this, SLOT(exFuncClicked()));
    connect(pb_sinX, SIGNAL(clicked()), this, SLOT(sinFuncClicked()));
    connect(pb_cosX, SIGNAL(clicked()), this, SLOT(cosFuncClicked()));
    connect(pb_lnOnePlusX, SIGNAL(clicked()), this, SLOT(lnFuncClicked()));
    connect(pb_onePlusXinA, SIGNAL(clicked()), this, SLOT(degFuncClicked()));
    connect(pb_arctgX, SIGNAL(clicked()), this, SLOT(arctgFuncClicked()));
    connect(pb_calculate, SIGNAL(clicked()), this, SLOT(calculateClicked()));
    connect(pb_onedivx, SIGNAL(clicked()), this, SLOT(oneDivXFuncClicked()));
    connect(pb_sinhx, SIGNAL(clicked()), this, SLOT(sinhFuncClicked()));
}

/*!
 * В зависимости от выбранной математической функции вызывается
 * функция для её вычисления
*/
void Widget::calculateClicked()
{
    switch (selectedFunction)
    {
    case einx:
        le_result->setText(QString::number(eInX(sb_xValue->value(), sb_eValue->value())));
        break;
    case sinx:
        le_result->setText(QString::number(sinX(sb_xValue->value(), sb_eValue->value())));
        break;
    case cosx:
        le_result->setText(QString::number(cosX(sb_xValue->value(), sb_eValue->value())));
        break;
    case lnx:
        le_result->setText(QString::number(lnX(sb_xValue->value(), sb_eValue->value())));
        break;
    case xina:
        le_result->setText(QString::number(xInA(sb_xValue->value(), sb_eValue->value(), sb_aValue->value())));
        break;
    case arctgx:
        le_result->setText(QString::number(arctgX(sb_xValue->value(), sb_eValue->value())));
        break;
    case onedivx:
        le_result->setText(QString::number(oneDivX(sb_xValue->value(), sb_eValue->value())));
        break;
    case sinhx:
        le_result->setText(QString::number(sinhX(sb_xValue->value(), sb_eValue->value())));
        break;
    }

}

/*!
 * Показывает виджеты в зависимости от выбранной математической функции
 * \param[in] func выбранная пользователем функция
 */
void Widget::showWidgets(int func)
{

    lbl_xValue->show();
    lbl_eValue->show();
    sb_xValue->show();
    sb_eValue->show();
    pb_calculate->setDisabled(false);
    if (func != xina)
    {
        sb_aValue->hide();
        lbl_aValue->hide();
    }
    else
    {
        sb_aValue->show();
        lbl_aValue->show();
    }
}

/*!
 * Устанавливает свойства виджетов для функции e^x
 */
void Widget::exFuncClicked()
{
    lbl_selectedFunction->setPixmap(QPixmap("X:\\QtProjects\\Macloren\\ex2.png"));
    selectedFunction = einx;
    showWidgets(selectedFunction);
    sb_xValue->setRange(-1000.0, 1000);
}
/*!
 * Устанавливает свойства виджетов для функции sinx
 */
void Widget::sinFuncClicked()
{
    lbl_selectedFunction->setPixmap(QPixmap("X:\\QtProjects\\Macloren\\sin2.png"));
    selectedFunction = sinx;
    showWidgets(selectedFunction);
    sb_xValue->setRange(-1000.0, 1000);
}
/*!
 * Устанавливает свойства виджетов для функции cosx
 */
void Widget::cosFuncClicked()
{
    lbl_selectedFunction->setPixmap(QPixmap("X:\\QtProjects\\Macloren\\cos2.png"));
    selectedFunction = cosx;
    showWidgets(selectedFunction);
    sb_xValue->setRange(-1000.0, 1000);
}
/*!
 * Устанавливает свойства виджетов для функции ln(1+x)
 */
void Widget::lnFuncClicked()
{
    lbl_selectedFunction->setPixmap(QPixmap("X:\\QtProjects\\Macloren\\ln2.png"));
    selectedFunction = lnx;
    showWidgets(selectedFunction);
    sb_xValue->setRange(-0.999, 0.999);
}
/*!
 * Устанавливает свойства виджетов для функции x^a
 */
void Widget::degFuncClicked()
{
    lbl_selectedFunction->setPixmap(QPixmap("X:\\QtProjects\\Macloren\\degA2.png"));
    selectedFunction = xina;
    showWidgets(selectedFunction);
    sb_xValue->setRange(-1000.0, 0.999);
}
/*!
 * Устанавливает свойства виджетов для функции arctgx
 */
void Widget::arctgFuncClicked()
{
    lbl_selectedFunction->setPixmap(QPixmap("X:\\QtProjects\\Macloren\\arctg2.png"));
    selectedFunction = arctgx;
    showWidgets(selectedFunction);
    sb_xValue->setRange(-1000.0, 1);
}
/*!
 * Устанавливает свойства виджетов для функции 1/x
 */
void Widget::oneDivXFuncClicked()
{
    lbl_selectedFunction->clear();
    selectedFunction = onedivx;
    showWidgets(selectedFunction);
    sb_xValue->setRange(-1000.0, 1);
}
/*!
 * Устанавливает свойства виджетов для функции sinhx
 */
void Widget::sinhFuncClicked()
{
    lbl_selectedFunction->clear();
    selectedFunction = sinhx;
    showWidgets(selectedFunction);
    sb_xValue->setRange(-1000.0, 1000);
}
Widget::~Widget()
{

}

